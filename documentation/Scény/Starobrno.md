# Starobrno
## Části
### Tunel
- Tunel vedoucí z Prahy (Braníku) do Brna
- Hráč sem bude dovezen důlním vozíkem, ale koleje nejsou dostaveny až do konce
	- Jízda naznačena vozíkem na levé straně obrazovky nebo filmeček
- Nepřátelé:
	- ~~Krysy - malé poškození, ale je jich hodně a jsou rychlé~~
	- ~~Pavouci - větší poškození, pomalejší~~
	- Krysí-lidé/pavoučí-lidé (jedno z toho, nepřátelé by měli být stejně vysocí jako hráč)
		- Útočí klackama/dřevěnými meči
- Objekty:
	- Důlní vozíky plné materiálu
	- Naskládané kolejnice (připravené k montáži)
	- Pražce (připravené k montáži)
- Pozadí:
	- Důlní stěna, kamenná/hliněná
	- Podpůrné sloupy
	- Olejové/plynové lampy

### Katakomby
- Oblast vypadající uspořádaněji než předešlé tunely, příhodně umístěná pod pivovarem
- Několik nosných sloupů, na které hráč položí trhavinu
- Nepřátelé:
	- Jako v předchozí oblasti
- Objekty:
	- Nosné sloupy - nelze rozbít úderem, pokládá se na ně trhavina
- Pozadí:
	- Skládaná kamenná stěna
	- Zdi zdobené lebkami/kostmi
	- Kostlivci ležící a zemi...
	- Pavučiny
- Konec oblasti je cesta ven z katakomb (dveře/žebřík)
### Město
- Může být už jen filmeček, jak Kryštof a Branice z dálky kouká na vybuchující pivovar