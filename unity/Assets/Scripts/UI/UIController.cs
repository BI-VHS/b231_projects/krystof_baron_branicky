﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField] private DialogueUI dialogueUI;
    [SerializeField] private TextMeshProUGUI livesText;
    [SerializeField] private TextMeshProUGUI moneyText;
    [SerializeField] private GameObject deathScreen;

    private void Start() {
        SetupPlayerDataUI();
    }

    public void UpdateLivesUI(int health, int maxHealth) {
        livesText.text = $"{health}/{maxHealth} zdraví";
    }

    public void UpdateMoneyUI(int money) {
        moneyText.text = $"{money} víček";
    }

    //public void UpdateHealthpacksUI(int healthpacks) {
    //    healthpacksText.text = $"{healthpacks} lékárniček";
    //}

    public void Death() {
        deathScreen.SetActive(true);
    }


    private void SetupPlayerDataUI() {
        GameObject gameStateGO = GameObject.FindGameObjectWithTag("GameState");
        if (gameStateGO == null) { Debug.LogError("No game state object found.", gameObject); return; }
        GameState gameState = gameStateGO.GetComponent<GameState>();
        if (gameState == null) { Debug.LogError("No game state script found.", gameObject); return; }
        PlayerState playerState = gameState.GetComponent<GameState>().GetPlayerState();
        if (gameState == null) { Debug.LogError("No player state found.", gameObject); return;  }
        
        playerState.livesChangeEvent.AddListener(UpdateLivesUI);
        playerState.moneyChangeEvent.AddListener(UpdateMoneyUI);
        //playerState.healthpacksChangeEvent.AddListener(UpdateHealthpacksUI);

        UpdateLivesUI(playerState.GetLives(), playerState.GetMaxLives());
        UpdateMoneyUI(playerState.GetMoney());
        //UpdateHealthpacksUI(playerState.GetHealthpacks());
    }
}
