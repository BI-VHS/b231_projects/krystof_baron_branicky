﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// Plays dialogue. Doesn't support audio yet.
/// </summary>
public class DialogueUI : MonoBehaviour {
    [SerializeField] private Image portrait;
    [SerializeField] private TextMeshProUGUI dialogueTextField;
    [SerializeField] private float letterDelay = 0.05f;
    [SerializeField] private List<TextMeshProUGUI> buttonLabels;
    [SerializeField] private GameObject textContainer;
    [SerializeField] private GameObject buttonContainer;

    [HideInInspector]
    public UnityEvent<string> dialogueEndEvent;

    private Coroutine textScroll;

    private DialogueSO dialogueData;
    private List<DialogueLine> currentLines;
    // Index of a next line
    private int nextLineIndex;
    private bool isDisplayingDialogue = false;
    private bool isEnding;
    private string output;
    private bool isFirstFrame = false;
    private Animator animator;
    private PlayerController playerController;

    private void Awake() {
        animator = GetComponent<Animator>();
    }

    private void Start() {
        GameObject playerGO = GameObject.FindGameObjectWithTag("Player");
        if (playerGO == null) Debug.LogError("Player not found", gameObject); 
        playerController = playerGO.GetComponent<PlayerController>();
    }

    private void Update() {
        if (!isDisplayingDialogue) { return; }

        if (!isFirstFrame && Input.GetKeyDown(KeyCode.E)) {
            if (textScroll != null) {
                StopCoroutine(textScroll);
                textScroll = null;
                ShowLine(currentLines[nextLineIndex - 1]);
            } else {
                NextLine();
            }
        }

        isFirstFrame = false;
    }


    /// <summary>
    ///     Setups and starts the dialogue.
    /// </summary>
    /// <returns>False if another dialogue is already happening, true otherwise.</returns>
    public bool StartDialogue(DialogueSO data) {

        if (isDisplayingDialogue) {
            //Debug.LogError("Dialogue box is being used!", gameObject);
            return false;
        }

        isDisplayingDialogue = true;

        SwitchToText();
        animator.SetBool("Activated", true);
        SetupDialogue(data);
        NextLine();

        isFirstFrame = true;

        playerController.SetControlBlocked(true);

        return true;

    }



    private void EndDialogue() {
        animator.SetBool("Activated", false);
        isDisplayingDialogue = false;
        playerController.SetControlBlocked(false);

        dialogueEndEvent.Invoke(output);
    }



    private void SwitchToText() {
        textContainer.SetActive(true);
        buttonContainer.SetActive(false);
    }

    private void SwitchToChoices() {
        textContainer.SetActive(false);
        buttonContainer.SetActive(true);
    }



    public void MakeChoice(int choice) {
        if (choice >= dialogueData.choices.Count) {
            return;
        }

        currentLines = dialogueData.choices[choice].dialogue;
        nextLineIndex = 0;
        isEnding = dialogueData.choices[choice].isEnding;
        if (isEnding) { output = dialogueData.choices[choice].output; }
        SwitchToText();
        NextLine();
    }



    public void NextLine() {

        if (nextLineIndex >= currentLines.Count) {
            if (isEnding) {
                EndDialogue();
            } else {
                SwitchToChoices();  
            }
            return;

        }

        portrait.sprite = currentLines[nextLineIndex].portrait;
        textScroll = StartCoroutine(PlayTextScroll(currentLines[nextLineIndex]));
        nextLineIndex++;
    }



    private IEnumerator PlayTextScroll(DialogueLine lineData) {
        dialogueTextField.text = lineData.speakerName + ": ";
        foreach (char c in lineData.text) { 
            yield return new WaitForSeconds(letterDelay);
            dialogueTextField.text += c;
        }

        textScroll = null;

    }



    private void ShowLine(DialogueLine lineData) {
        dialogueTextField.text = lineData.speakerName + ": ";
        dialogueTextField.text += lineData.text;
    }



    private void SetupDialogue(DialogueSO data) {
        dialogueData = data;
        currentLines = dialogueData.entryLines;
        nextLineIndex = 0;
        output = "";

        if (dialogueData.choices.Count == 0) {
            isEnding = true;
        } else {
            isEnding = false;
        }

        SetChoiceLabels();

    }



    private void SetChoiceLabels() {
        for (int i = 0; i < buttonLabels.Count; i++) {
            if (i < dialogueData.choices.Count) {
                buttonLabels[i].text = "-> " + dialogueData.choices[i].choiceText;
            } else {
                buttonLabels[i].text = "";
            }

        }
    }


}
