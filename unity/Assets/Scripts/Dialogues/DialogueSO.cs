using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dialogues/Dialogue", fileName = "NewDialogue")]
public class DialogueSO : ScriptableObject {
    public List<DialogueLine> entryLines;
    public List<DialogueChoice> choices;
}
