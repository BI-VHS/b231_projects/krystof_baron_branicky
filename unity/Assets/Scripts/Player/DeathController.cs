﻿using System.Collections;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class DeathController : MonoBehaviour {

    private int moneyOnSceneLoad;

    PlayerState ps;

    // Use this for initialization
    void Start() {
        GameState gameState = GameObject.FindGameObjectWithTag("GameState").GetComponent<GameState>();
        if (gameState == null) { Debug.LogError("No game state found."); return; }

        ps = gameState.GetPlayerState();
        moneyOnSceneLoad = ps.GetMoney();
    }

    public void PlayerDeath() {
        ps.SetMoney(moneyOnSceneLoad);
        ps.SetLives(ps.GetMaxLives());
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}
