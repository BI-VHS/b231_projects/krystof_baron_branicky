﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Bartender : MonoBehaviour
{
    [SerializeField] private NpcDialogue defaultDialogue;
    [SerializeField] private NpcDialogue servingOtherBeer;
    [SerializeField] private NpcDialogue servingBranikBeer;
    [SerializeField] private NpcDialogue chillRoomDialogue;
    [SerializeField] private Transform barNavNode;
    [SerializeField] private Transform barellNavNode;
    [SerializeField] private Transform chillRoomNavNode;
    [SerializeField] private Navigator navigator;
    [SerializeField] private Animator animator;
    [SerializeField] private Trigger dialogueTrigger;

    private BartenderState bState;
    private int pouredBeerCount = 0;
    private bool movingStateBeforeDailogue = false;

    private BranikState branikState;
    private NpcJobs job;


    private void Awake() {
        job = GetComponent<NpcJobs>();
    }

    private void Start() {
        GameState gameState = GameObject.FindGameObjectWithTag("GameState").GetComponent<GameState>();
        branikState = gameState.GetBranikState();
        job.jobDoneEvent.AddListener(EndState);
        job.Teleport(barNavNode.position);
        dialogueTrigger.activationEvent.AddListener(Talk);

        Debug.Log(navigator.GetPath(barNavNode.position, barNavNode.position).Count + "is the lenght of path where start=end");

        ChangeState(BartenderState.idleAtBar);
    }

    public void Talk() {
        List<NpcDialogue> availableDialogue = new();

        if (branikState.AllMachinesOwned())
            availableDialogue.Add(servingBranikBeer);
        if (!branikState.AllMachinesOwned())
            availableDialogue.Add(servingOtherBeer);
        if (bState == BartenderState.relaxing)
            availableDialogue.Add(chillRoomDialogue);

        availableDialogue.Add(defaultDialogue);

        dialogueTrigger.SetIsEnabled(false);
        movingStateBeforeDailogue = animator.GetBool("IsWalking");
        animator.SetBool("IsWalking", false);
        job.dialogueOutputEvent.AddListener(EnableDialogue);
        job.PlayDialogue(availableDialogue);
    }


    public void EndState() {
        switch (bState) {
            case BartenderState.idleAtBar:
                if (Random.Range(0f, 1f) < 0.1) {
                    ChangeState(BartenderState.walkingToRelaxRoom);
                } else {
                    ChangeState(BartenderState.pouringBeer);
                }
                break;

            case BartenderState.pouringBeer:
                if (pouredBeerCount < 3) {
                    pouredBeerCount++;
                    ChangeState(BartenderState.idleAtBar);
                }
                else {
                    pouredBeerCount = 0;
                    ChangeState(BartenderState.walkingForRefill);
                }
                break;

            case BartenderState.walkingForRefill:
                ChangeState(BartenderState.pickingUpBarell);
                break;

            case BartenderState.pickingUpBarell:
                ChangeState(BartenderState.walkingTowardsBar);
                break;

            case BartenderState.walkingTowardsBar:
                ChangeState(BartenderState.idleAtBar);
                break;

            case BartenderState.walkingToRelaxRoom:
                ChangeState(BartenderState.relaxing);
                break;

            case BartenderState.relaxing:
                ChangeState(BartenderState.walkingTowardsBar);
                break;

        }
    }

    private void ChangeState(BartenderState newState) {
        bState = newState;
        switch (bState) {
            case BartenderState.idleAtBar:
                animator.SetBool("IsWalking", false);
                job.DisplayString("");
                job.Wait(8f);
                break;

            case BartenderState.pouringBeer:
                animator.SetBool("IsWalking", false);
                job.DisplayString("Nalévá pivo");
                job.Wait(5f);
                break;

            case BartenderState.walkingForRefill:
                animator.SetBool("IsWalking", true);
                job.DisplayString("");
                job.FollowPath(navigator.GetPathFromMe(transform.position, barellNavNode.position));
                break;

            case BartenderState.pickingUpBarell:
                animator.SetBool("IsWalking", false);
                job.DisplayString("Zvedá barel");
                job.Wait(3);
                break;

            case BartenderState.walkingTowardsBar:
                animator.SetBool("IsWalking", true);
                job.DisplayString("");
                job.FollowPath(navigator.GetPathFromMe(transform.position, barNavNode.position));
                break;

            case BartenderState.walkingToRelaxRoom:
                animator.SetBool("IsWalking", true);
                job.DisplayString("");
                job.FollowPath(navigator.GetPathFromMe(transform.position, chillRoomNavNode.position));
                break;

            case BartenderState.relaxing:
                animator.SetBool("IsWalking", false);
                job.DisplayString("Odpočívá");
                job.Wait(40);
                break;
        }
    }


    private void EnableDialogue(string output) {
        animator.SetBool("IsWalking", movingStateBeforeDailogue);
        dialogueTrigger.SetIsEnabled(true);
    }



    private enum BartenderState {
        idleAtBar,
        pouringBeer,
        walkingForRefill,
        pickingUpBarell,
        walkingTowardsBar,
        walkingToRelaxRoom,
        relaxing,
    }

}
