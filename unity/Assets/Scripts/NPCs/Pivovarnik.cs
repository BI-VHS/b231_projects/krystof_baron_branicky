﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(NpcJobs))]
public class Pivovarnik : MonoBehaviour
{
    [SerializeField] private NpcDialogue defaultDialogue;
    [SerializeField] private NpcDialogue branikBrokenDialogue;
    [SerializeField] private NpcDialogue branikWorkingDialogue;
    [SerializeField] private NpcDialogue breakRoomDialogue;
    [SerializeField] private NpcDialogue peeingDialogue;
    [SerializeField] private List<Transform> machineNavNodes;
    [SerializeField] private Transform breakRoomNavNode;
    [SerializeField] private Transform toiletsNavNode;
    [SerializeField] private Navigator navigator;
    [SerializeField] private Animator animator;
    [SerializeField] private Trigger dialogueTrigger;

    private PivovarnikState state;
    private bool movingStateBeforeDailogue = false;

    private BranikState branikState;
    private NpcJobs job;


    private void Awake() {
        job = GetComponent<NpcJobs>();
    }

    private void Start() {
        GameState gameState = GameObject.FindGameObjectWithTag("GameState").GetComponent<GameState>();
        branikState = gameState.GetBranikState();
        job.jobDoneEvent.AddListener(EndState);

        dialogueTrigger.activationEvent.AddListener(Talk);
        
        if (branikState.AllMachinesOwned()) {
            job.Teleport(machineNavNodes[0].position);
            ChangeState(PivovarnikState.goingToMachine);
        } else {
            job.Teleport(breakRoomNavNode.position);
            branikState.changeEvent.AddListener(ChangeBehaviour);
        }

    }

    public void Talk() {
        List<NpcDialogue> availableDialogue = new();

        if (state == PivovarnikState.peeing)
            availableDialogue.Add(peeingDialogue);
        if (branikState.AllMachinesOwned())
            availableDialogue.Add(branikWorkingDialogue);
        if (!branikState.AllMachinesOwned())
            availableDialogue.Add(branikBrokenDialogue);
        if (state == PivovarnikState.relaxing)
            availableDialogue.Add(breakRoomDialogue);

        availableDialogue.Add(defaultDialogue);

        dialogueTrigger.SetIsEnabled(false);
        movingStateBeforeDailogue = animator.GetBool("IsWalking");
        animator.SetBool("IsWalking", false);
        job.dialogueOutputEvent.AddListener(EnableDialogue);
        job.PlayDialogue(availableDialogue);
    }


    public void EndState() {
        switch (state) {
            case PivovarnikState.operatingMachine:
                switch (Random.Range(1, 6)) {
                    case 1: ChangeState(PivovarnikState.goingToToilets); break;
                    case 2: ChangeState(PivovarnikState.goingToBreakRoom); break;
                    default: ChangeState(PivovarnikState.goingToMachine); break; 
                }
                break;

            case PivovarnikState.goingToMachine:
                ChangeState(PivovarnikState.operatingMachine);
                break;

            case PivovarnikState.goingToToilets:
                ChangeState(PivovarnikState.peeing);
                break;

            case PivovarnikState.peeing:
                ChangeState(PivovarnikState.goingToMachine);
                break;

            case PivovarnikState.goingToBreakRoom:
                ChangeState(PivovarnikState.relaxing);
                break;

            case PivovarnikState.relaxing:
                ChangeState(PivovarnikState.goingToMachine);
                break;
        }
    }

    private void ChangeState(PivovarnikState newState) {
        state = newState;
                
        switch (state) {

            case PivovarnikState.operatingMachine:
                animator.SetBool("IsWalking", false);
                job.DisplayString("Pracuje");
                job.Wait(15);
                break;

            case PivovarnikState.goingToMachine:
                animator.SetBool("IsWalking", true);
                job.DisplayString("");
                job.FollowPath(navigator.GetPathFromMe(transform.position, GetRandomMachine().position));
                break;

            case PivovarnikState.goingToToilets:
                animator.SetBool("IsWalking", true);
                job.DisplayString("");
                job.FollowPath(navigator.GetPathFromMe(transform.position, toiletsNavNode.position));
                break;

            case PivovarnikState.peeing:
                animator.SetBool("IsWalking", false);
                job.DisplayString("Močí");
                job.Wait(20);
                break;

            case PivovarnikState.goingToBreakRoom:
                animator.SetBool("IsWalking", true);
                job.DisplayString("");
                job.FollowPath(navigator.GetPathFromMe(transform.position, breakRoomNavNode.position));
                break;

            case PivovarnikState.relaxing:
                animator.SetBool("IsWalking", false);
                job.DisplayString("Odpočívá");
                job.Wait(50);
                break;
        }
    }


    private void EnableDialogue(string output) {
        animator.SetBool("IsWalking", movingStateBeforeDailogue);
        dialogueTrigger.SetIsEnabled(true);
    }

    private Transform GetRandomMachine() {
        return machineNavNodes[Random.Range(0, machineNavNodes.Count)];
    }

    /// <summary>
    ///     Changes behaviour from doing nothing to doing something. If all machines are owned.
    /// </summary>
    private void ChangeBehaviour() {
        if (branikState.AllMachinesOwned()) {
            ChangeState(PivovarnikState.goingToMachine);
        }
    }



    private enum PivovarnikState {
        operatingMachine,
        goingToMachine,
        goingToToilets,
        peeing,
        goingToBreakRoom,
        relaxing,
    }

}
