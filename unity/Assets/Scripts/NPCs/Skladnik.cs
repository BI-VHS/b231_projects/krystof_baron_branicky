﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(NpcJobs))]
public class Skladnik : MonoBehaviour
{
    [SerializeField] private NpcDialogue defaultDialogue;
    [SerializeField] private NpcDialogue branikBrokenDialogue;
    [SerializeField] private NpcDialogue branikWorkingDialogue;
    [SerializeField] private NpcDialogue breakRoomDialogue;
    [SerializeField] private List<Transform> boxNavNodes;
    [SerializeField] private Transform gristmillNavNode;
    [SerializeField] private Transform breakRoomNavNode;
    [SerializeField] private Navigator navigator;
    [SerializeField] private Animator animator;
    [SerializeField] private Trigger dialogueTrigger;

    private SkladnikState state;
    private bool movingStateBeforeDailogue = false;

    private BranikState branikState;
    private NpcJobs job;


    private void Awake() {
        job = GetComponent<NpcJobs>();
    }

    private void Start() {
        GameState gameState = GameObject.FindGameObjectWithTag("GameState").GetComponent<GameState>();
        branikState = gameState.GetBranikState();
        job.jobDoneEvent.AddListener(EndState);

        dialogueTrigger.activationEvent.AddListener(Talk);
        
        if (branikState.AllMachinesOwned()) {
            job.Teleport(boxNavNodes[0].position);
            ChangeState(SkladnikState.goingToBoxes);
        } else {
            job.Teleport(breakRoomNavNode.position);
            branikState.changeEvent.AddListener(ChangeBehaviour);
        }

    }

    public void Talk() {
        List<NpcDialogue> availableDialogue = new();

        if (branikState.AllMachinesOwned())
            availableDialogue.Add(branikWorkingDialogue);
        if (!branikState.AllMachinesOwned())
            availableDialogue.Add(branikBrokenDialogue);
        if (state == SkladnikState.relaxing)
            availableDialogue.Add(breakRoomDialogue);

        availableDialogue.Add(defaultDialogue);

        dialogueTrigger.SetIsEnabled(false);
        movingStateBeforeDailogue = animator.GetBool("IsWalking");
        animator.SetBool("IsWalking", false);
        job.dialogueOutputEvent.AddListener(EnableDialogue);
        job.PlayDialogue(availableDialogue);
    }


    public void EndState() {
        switch (state) {
            case SkladnikState.managingBoxes:
                switch (Random.Range(1, 6)) {
                    case 1: ChangeState(SkladnikState.goingToGristmill); break;
                    case 2: ChangeState(SkladnikState.goingToBreakRoom); break;
                    default: ChangeState(SkladnikState.goingToBoxes); break; 
                }
                break;

            case SkladnikState.goingToBoxes:
                ChangeState(SkladnikState.managingBoxes);
                break;

            case SkladnikState.goingToGristmill:
                ChangeState(SkladnikState.pouringGrain);
                break;

            case SkladnikState.pouringGrain:
                ChangeState(SkladnikState.goingToBoxes);
                break;

            case SkladnikState.goingToBreakRoom:
                ChangeState(SkladnikState.relaxing);
                break;

            case SkladnikState.relaxing:
                ChangeState(SkladnikState.goingToBoxes);
                break;
        }
    }

    private void ChangeState(SkladnikState newState) {
        state = newState;
                
        switch (state) {

            case SkladnikState.managingBoxes:
                animator.SetBool("IsWalking", false);
                job.DisplayString("Přerovnává krabice");
                job.Wait(10);
                break;

            case SkladnikState.goingToBoxes:
                animator.SetBool("IsWalking", true);
                job.DisplayString("");
                job.FollowPath(navigator.GetPathFromMe(transform.position, GetRandomBox().position));
                break;

            case SkladnikState.goingToGristmill:
                animator.SetBool("IsWalking", true);
                job.DisplayString("");
                job.FollowPath(navigator.GetPathFromMe(transform.position, gristmillNavNode.position));
                break;

            case SkladnikState.pouringGrain:
                animator.SetBool("IsWalking", false);
                job.DisplayString("Sype slad");
                job.Wait(10);
                break;

            case SkladnikState.goingToBreakRoom:
                animator.SetBool("IsWalking", true);
                job.DisplayString("");
                job.FollowPath(navigator.GetPathFromMe(transform.position, breakRoomNavNode.position));
                break;

            case SkladnikState.relaxing:
                animator.SetBool("IsWalking", false);
                job.DisplayString("Odpočívá");
                job.Wait(50);
                break;
        }
    }


    private void EnableDialogue(string output) {
        animator.SetBool("IsWalking", movingStateBeforeDailogue);
        dialogueTrigger.SetIsEnabled(true);
    }

    private Transform GetRandomBox() {
        return boxNavNodes[Random.Range(0, boxNavNodes.Count)];
    }

    /// <summary>
    ///     Changes behaviour from doing nothing to doing something. If all machines are owned.
    /// </summary>
    private void ChangeBehaviour() {
        if (branikState.AllMachinesOwned()) {
            ChangeState(SkladnikState.goingToBoxes);
        }
    }



    private enum SkladnikState {
        managingBoxes,
        goingToBoxes,
        goingToGristmill,
        pouringGrain,
        goingToBreakRoom,
        relaxing,
    }

}
