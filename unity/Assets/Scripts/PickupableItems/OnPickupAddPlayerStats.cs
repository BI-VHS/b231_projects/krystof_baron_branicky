﻿using UnityEngine;

[RequireComponent (typeof(LootItem))]
public class OnPickupAddPlayerStats : MonoBehaviour {

    [SerializeField] int moneyIncrease = 0;
    [SerializeField] int livesIncrease = 0;

    private PlayerState playerState;

    private void Start() {
        GameState gameState = GameObject.FindGameObjectWithTag("GameState").GetComponent<GameState>();
        if (gameState == null) { Debug.LogError("No game state found."); return; }

        playerState = gameState.GetPlayerState();
        GetComponent<LootItem>().pickupEvent.AddListener(AddStats);
    }

    private void AddStats() {
        playerState.ChangeMoney(moneyIncrease);
        playerState.ChangeLives(livesIncrease);
    }
}