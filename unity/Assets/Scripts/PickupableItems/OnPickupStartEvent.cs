﻿using UnityEngine;

public class OnPickupStartEvent : MonoBehaviour {

    [SerializeField] private string eventObjectName;

    private void Start() {
        GetComponent<LootItem>().pickupEvent.AddListener(StartEvent);
    }

    private void StartEvent() {
        GameObject.Find(eventObjectName).GetComponent<EventController>().StartEvent();
    }
}