using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Stores all persistent data.
/// </summary>
[Serializable]
public class GameState : MonoBehaviour {
    private PlayerState playerState = new();
    private BranikState branikState = new();
    [SerializeField] private MissionsState missionsState;

    private void Awake() {
        // Ensure only one copy of this object exists
        //Debug.Log("Gamestate Awake!", gameObject);
        GameObject[] objs = GameObject.FindGameObjectsWithTag("GameState");

        if (objs.Length > 1) {
            //Debug.Log("Gamestate Destroyed!", gameObject);
            Destroy(this.gameObject);
            return;
        }

        //Debug.Log("Gamestate Persists!", gameObject);
        DontDestroyOnLoad(this.gameObject);
    }



    public PlayerState GetPlayerState() { return playerState; }

    public BranikState GetBranikState() { return branikState; }

    public MissionsState GetMissionsState() {  return missionsState; }





    [ContextMenu("Completed missions")]
    private void ContextMenuCompletedMissions() {
        Debug.Log(missionsState.MissionsToString());
    }
}
