using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class PlayerState {
    public UnityEvent<int, int> livesChangeEvent = new();
    public UnityEvent<int> moneyChangeEvent = new();
    public UnityEvent<int> healthpacksChangeEvent = new();

    private int maxLives = 10; 
    private int lives = 10; 
    private int money = 0; 
    private int healthpacks = 0;

    public int GetMaxLives() { return maxLives; }

    public int GetLives() { return lives; }

    public int GetMoney() { return money;}

    public int GetHealthpacks() {  return healthpacks; }

    public bool ChangeMoney(int amount) {
        if (money + amount < 0) {
            return false;
        }

        this.money += amount;
        moneyChangeEvent.Invoke(money);
        return true;
    }

    public bool ChangeHealthpacks(int amount) {  
        if (healthpacks + amount < 0) {
            return false;
        }

        this.healthpacks += amount;
        healthpacksChangeEvent.Invoke(healthpacks);
        return true;
    }

    public void ChangeLives(int amount) {
        lives += amount;
        if (lives < 0) {
            lives = 0;
        }
        if (lives > maxLives) {
            lives = maxLives;
        }
        livesChangeEvent.Invoke(lives, maxLives);
    }

    public void SetMaxLives(int maxLives) {
        this.maxLives = maxLives;
        livesChangeEvent.Invoke(lives, maxLives);
    }

    public void SetLives(int lives) {
        this.lives = lives;
        livesChangeEvent.Invoke(lives, maxLives);
    }

    public void SetMoney(int money) { 
        this.money = money;
        moneyChangeEvent.Invoke(money);
    }

    public void SetHealthpacks(int healthpacks) {
        this.healthpacks = healthpacks;
        healthpacksChangeEvent.Invoke(healthpacks);
    }
}
